#TODO: check for strings.

import re
import copy

def main(arg):
	return step(arg)+'\n'+str(len(arg.split('\n')))+'£'

def modify(arg,line,value):
	argu = copy.deepcopy(arg)
	argu[line] = value
	return '\n'.join(argu)

def step(arg):
	lines = arg.split('\n')
	for j in range(len(lines)):
		i = lines[j]
		if len(re.findall(r'\(([^\(\)]*)\)',i)) != 0:
			prefix = modify(lines,j,re.sub(r'\(([^\(\)]*)\)',str(len(lines)+1)+'£',i,1))
			suffix = re.findall(r'\(([^\(\)]*)\)',i)[0]
			return step(prefix + '\n' + suffix)
	return arg